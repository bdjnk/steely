#![cfg_attr(
  all(not(debug_assertions), target_os = "windows"),
  windows_subsystem = "windows"
)]

extern crate native_tls;

use native_tls::TlsConnector;
use std::io::{ Read, Write };
use std::net::TcpStream;
use url::Url;

#[tauri::command]
fn fetch_gemini(url_str: String) -> String {

  let url = &Url::parse(&url_str);
  let url = match url {
    Ok(url) => url,
    Err(error) => return error.to_string()
  };
  let host = match url.host_str() {
    Some(host) => host,
    None => return "No host provided.".to_string()
  };

  let stream = match TcpStream::connect(format!("{}:1965", host)) {
    Ok(stream) => stream,
    Err(error) => return error.to_string()
  };

  let connector = match TlsConnector::builder().danger_accept_invalid_certs(true).build() {
    Ok(connector) => connector,
    Err(error) => return error.to_string()
  };
  let mut stream = match connector.connect(host, stream) {
    Ok(stream) => stream,
    Err(error) => return error.to_string()
  };

  match stream.write_all(format!("{}\r\n", url).as_bytes()) {
    Err(error) => return error.to_string(),
    _ => ()
  };

  let mut string_read = String::new();
  match stream.read_to_string(&mut string_read) {
    Err(error) => return error.to_string(),
    _ => ()
  };

  string_read
}

fn main() {
  tauri::Builder::default()
    .invoke_handler(tauri::generate_handler![fetch_gemini])
    .run(tauri::generate_context!())
    .expect("error while running tauri application");
}
