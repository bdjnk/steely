import "solid-js";
import { render } from 'solid-js/web';

import './style/index.css';
import App from './App';

render(App, document.getElementById('root') as Node);
