import { tauri, shell } from '@tauri-apps/api'
import { createSignal, createEffect, Show, For } from 'solid-js';
import { Portal } from 'solid-js/web'
import * as URI from 'uri-js';

import './style/App.css';
import parse from './utils/parse.js';

import Controls from './components/Controls';



async function external(uri: string) {
  console.log('opening', uri);
  await shell.open(uri);
}

let redirectCount = 0;

const App = () => {
  const [geminiUrl, setGeminiUrl] = createSignal('');
  const [gemTokens, setGemTokens] = createSignal([]);
  const [special, setSpecial]: [Function, Function] = createSignal(null);

  const getComponent = token => {
    switch (token.type) {
      case 'quote':
        return <blockquote>{token.content}</blockquote>;
      case 'header': {
        switch (token.level) {
          case 1: return <h1>{token.content}</h1>;
          case 2: return <h2>{token.content}</h2>;
          case 3: return <h3>{token.content}</h3>;
        }
      }
      case 'link': {
        const resolved = URI.resolve(geminiUrl(), token.href, { scheme: 'https' });
        return <a href={`#${resolved}`}>{token.content}</a>;
      }
      case 'pre':
        return token.alt
          ? <pre><code class={`language-${token.alt}`}>{token.items.join('\n')}</code></pre>
          : <pre>{token.items.join('\n')}</pre>;
      case 'list':
        return <ul>{token.items.map((item: string) => <li>{item}</li>)}</ul>;
      default:
        return token.content ? <p dir="auto">{token.content}</p> : <br/>;
    }
  };

  const cancelSpecial = () => {
    history.back();
    setSpecial(null);
  }

  const submitInput = () => {
    const input = document.getElementById('input')?.value;
    if (!input) return;
    const href = URI.resolve(geminiUrl(), '?'+input);
    console.log('submitting', input, href);
    setGeminiUrl(href);
    setSpecial(null);
    history.replaceState(null, '', '#'+href);
  }

  createEffect(() => {
    const urlStr = geminiUrl();
    console.log('fetching', urlStr);
    if (!urlStr) return;

    tauri.invoke('fetch_gemini', { urlStr }).then(response => {
      const gemtext = response as string;

      const header = gemtext.substring(0, gemtext.indexOf("\n"));
      console.log('fetched', header);

      const code = header.substring(0, 2);
      const meta = header.substring(2).trim();

      if (code.substring(0,1) !== '3') redirectCount = 0;

      switch (code) {
        case '10':   // INPUT
        case '11': { // SENSITIVE INPUT
          setSpecial(
            <>
              <h1>{meta}</h1>
              <input id="input" type={code === '11' ? 'password' : 'text'} />
              <div class="actions">
                <button onClick={cancelSpecial}>Cancel</button>
                <button onClick={submitInput}>Submit</button>
              </div>
            </>
          );
          break;
        }
        case '20': { // SUCCESS
          const content = gemtext.substring(gemtext.indexOf("\n") + 1).trim();
          const tokens = parse(content);
          setGemTokens(tokens);
          break;
        }
        case '30':   // REDIRECT - TEMPORARY
        case '31': { // REDIRECT - PERMANENT
          if (redirectCount++ < 3) {
            const next = URI.resolve(geminiUrl(), meta);
            setGeminiUrl(next);
            history.replaceState(null, '', '#'+next);
          } else {
            setSpecial(
              <>
                <h1>Too Many Redirects</h1>
                <div class="actions">
                  <button onClick={cancelSpecial}>Okay</button>
                </div>
              </>
            );
          }
          break;
        }
        default: // UNKNOWN
      }
    });
  });
  
  addEventListener("hashchange", () => {
    setSpecial(null);
    const href = location.hash.substring(1);
    const uri = URI.parse(href);
    console.log('hashchange', href);
    if (uri.reference === 'absolute' && uri.scheme !== 'gemini') {
      external(href);
      return;
    }
    setGeminiUrl(href);
  });

  location.hash = 'gemini://gemini.conman.org/test/torture/';

  return (
    <>
      <Controls url={geminiUrl()} />
      <div class="page">
        <Show when={special() == null} fallback={special()}>
          <For each={gemTokens()}>
            {token => getComponent(token)}
          </For>
        </Show>
      </div>
    </>
  );
}

export default App;
