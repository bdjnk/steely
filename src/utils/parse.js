const HEADER = /^(#{1,3})\s*(.*)/
const PRE = /^```(.*)?/
const LIST = /^\*\s*(.*)/
const QUOTE = /^>\s*(.*)/
const LINK = /^=>\s+(\S+)\s*(.*)/

const parse = text => {
  const lines = text.split(/\r?\n/)

  const tokens = []

  let index = 0

  while (index < lines.length) {
    const line = lines[index]
    let match = null

    if (match = line.match(HEADER)) {
      const [_, levels, content] = match

      tokens.push({ type: 'header', level: levels.length, content })
    } else
    if (match = line.match(PRE)) {
      const [_, alt] = match
      const items = []
      index++

      while (index < lines.length) {
        const item = lines[index]
        if (lines[index].match(PRE)) break
        items.push(item)
        index++
      }

      tokens.push({ type: 'pre', items, alt })
    } else
    if (match = line.match(LIST)) {
      const items = []

      while (index < lines.length) {
        const match = lines[index].match(LIST)
        if (!match) break
        const [_, item] = match
        items.push(item)
        index++
      }
      index--

      tokens.push({ type: 'list', items })
    } else
    if (match = line.match(QUOTE)) {
      const [_, content] = match

      tokens.push({ type: 'quote', content })
    } else
    if (match = line.match(LINK)) {
      const [_, href, rawContent] = match
      const content = rawContent || href

      tokens.push({ type: 'link', content, href })
    } else {
      tokens.push({ type: 'text', content: line })
    }

    index++
  }

  return tokens
}

export default parse
