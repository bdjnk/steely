A steely-eyed Gemini client.

In pre-alpha. Do not use.

That said, to use this run `npm run start` and while that is running run `npm run tauri dev`.

Uses [Tauri](https://tauri.studio/), [Solid](https://dev.solidjs.com/), [uri-js](https://github.com/garycourt/uri-js), and [gemini-to-html](https://github.com/RangerMauve/gemini-to-html).